#include "track.h"
#include <stdexcept>
#include <algorithm>
namespace thwo {

Track::Track(const jsoncons::json &data) {
  const auto &race = data["race"];
  const auto &track = race["track"];

  std::string idj = track["id"].as<std::string>();
  std::string namej = track["name"].as<std::string>();

  id = std::move(idj);
  name = std::move(namej);

  const auto &piecesj = track["pieces"];

  for (auto it = piecesj.begin_elements(); it != piecesj.end_elements(); ++it) {
    if (it->has_member("length")) {
      double length = (*it)["length"].as<double>();
      bool hasSwitch = it->has_member("switch");
      pieces.push_back(Track::piece{ length, hasSwitch, false, 0.0, 0.0 });
    } else {
      double radius = (*it)["radius"].as<double>();
      double angle = (*it)["angle"].as<double>();
      bool hasSwitch = it->has_member("switch");
      pieces.push_back(Track::piece{ 0.0, hasSwitch, true, radius, angle });
    }
  }

  const auto &lanesj = track["lanes"];

  int min,max;
  min = max = 0;

  for (auto it = lanesj.begin_elements(); it != lanesj.end_elements(); ++it) {

    double dist = (*it)["distanceFromCenter"].as<double>();
    int index = (*it)["index"].as<int>();
    if (index > max) max = index;
    if (index < min) min = index;
    lanes[index] = dist;
  }

  leftLaneId = min;
  rightLaneId = max;
}

int Track::getNumberOfPieces() const { return pieces.size(); }

Track::piece Track::getPiece(int pieceId) const { return pieces.at(pieceId); }

double Track::distance(int pieceID1, double inPieceDistance1, int pieceID2,
                       double inPieceDistance2, int laneID1,
                       int laneID2) const {
  double dist{ 0.0 };
  if (pieceID1 == pieceID2) {
    if (inPieceDistance1 > inPieceDistance2) {
      throw std::invalid_argument("Negative distance.");
    }
    dist += distanceInPiece(pieceID1, inPieceDistance1, inPieceDistance2);
  } else {
    dist += distanceToEnd(pieceID1, inPieceDistance1, laneID1);
    dist += distanceFromBegin(pieceID2, inPieceDistance2);
  }
  return dist;
}

double Track::distanceInPiece(int pieceID, double inDistance1,
                              double inDistance2) const {
  return inDistance2 - inDistance1;
}

double Track::distanceFromBegin(int pieceID, double inDistance) const {
  return distanceInPiece(pieceID, 0.0, inDistance);
}

double Track::distanceToEnd(int pieceID, double inDistance, int laneID) const {
  if (pieces[pieceID].isBend) {
    const double pi{ 3.14159265 };
    double length = (pieces[pieceID].radius + laneDistanceFromCentre(laneID)) *
                    pi * pieces[pieceID].angle / 180.0;
    return distanceInPiece(pieceID, inDistance, length);
  } else {
    return pieces[pieceID].length - inDistance;
  }
}

int Track::getLeftLaneId() const { return leftLaneId; }

int Track::getRightLaneId() const { return rightLaneId; }

double Track::laneDistanceFromCentre(int laneId) const {
  return lanes.at(laneId);
}

} // namespace thwo
