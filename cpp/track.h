#ifndef T_TRACK_H
#define T_TRACK_H

#include <map>
#include <vector>
#include <string>
#include <jsoncons/json.hpp>

namespace thwo {

class Track {
public:
  struct piece {
    double length;
    bool hasSwitch;
    bool isBend;
    double radius;
    double angle;
  };

  Track(const jsoncons::json &msg);
  Track() = default;
  Track(Track&& that) = default;
  Track& operator=(Track&& other) = default;

  int getNumberOfPieces() const;

  piece getPiece(int pieceId) const;

  double distance(int pieceID1, double inPieceDistance1, int pieceID2,
                  double inPieceDistance2, int laneID1, int laneID2) const;

  int getLeftLaneId() const;

  int getRightLaneId() const;

  double laneDistanceFromCentre(int laneId) const;

private:
  double distanceInPiece(int pieceID, double inDistance1,
                         double inDistance2) const;

  double distanceFromBegin(int pieceID, double inDistance) const;

  double distanceToEnd(int pieceID, double inDistance, int laneID) const;

  std::vector<piece> pieces;

  std::map<int, double> lanes;
  int leftLaneId;
  int rightLaneId;
  std::string id;
  std::string name;
};

} // namespace thwo
#endif 
