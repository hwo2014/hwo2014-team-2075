#ifndef NNPROJECT_MLP
#define NNPROJECT_MLP
#include <algorithm>
#include <string>
#include <vector>
#include <random>
#include <cmath>
#include <chrono>
#include <fstream>
#include <iostream>
#include <Eigen/Dense>
#include <Eigen/Core>
using std::cout;
using std::ofstream;
using std::ifstream;
using std::endl;
using std::string;
using std::vector;
using std::pair;
using std::random_shuffle;
using namespace std::chrono;
using Eigen::VectorXf;
using Eigen::MatrixXf;
using Eigen::Array;

class MLP {
  int inputs;
  int hidden;
  int outputs;
  float learning_rate;
  MatrixXf Wh;
  MatrixXf Wo;
  VectorXf Yh;
  VectorXf Do;
  MatrixXf Dh;
  MatrixXf DWo;
  MatrixXf DWh;
  vector<float> error_train;
  int epochs;
  VectorXf activation(VectorXf &u);
 public:
  MLP(int inputs, int hidden, int outputs, float n);
  MLP(string filename);
  VectorXf forward(const VectorXf &x);
  void backward(const VectorXf &desired, const VectorXf &output,
                       const MatrixXf &input);
  void train(const vector<VectorXf> &traindata,
             const vector<VectorXf> &trainlabels, int epochs);
  VectorXf regress(const VectorXf &input);
  int classify(const VectorXf &input);
  float MSE(const vector<pair<VectorXf, VectorXf> > &data);
  void save_network(string filename);
};
#endif
