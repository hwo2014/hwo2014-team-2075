#include "game_logic.h"
#include "protocol.h"
#include <Eigen/Core>
#include <Eigen/Dense>
#include <vector>
#include <utility>
#include <cmath>
#include <random>
#include <chrono>

using namespace std::chrono;
using std::vector;
using std::pair;
using Eigen::VectorXf;

using namespace hwo_protocol;


game_logic::game_logic()
  : action_map
    {
      { "join", &game_logic::on_join },
      { "gameStart", &game_logic::on_game_start },
      { "carPositions", &game_logic::on_car_positions },
      { "crash", &game_logic::on_crash },
      { "gameEnd", &game_logic::on_game_end },
      { "error", &game_logic::on_error },
      { "gameInit", &game_logic::on_game_init },
      { "yourCar", &game_logic::on_your_car }
    }
{
}

game_logic::msg_vector game_logic::react(const jsoncons::json& msg)
{
  const auto& msg_type = msg["msgType"].as<std::string>();
  const auto& data = msg["data"];
  if (msg.has_member("gameTick"))
    currentTick = msg["gameTick"].as<int>();

  auto action_it = action_map.find(msg_type);
  if (action_it != action_map.end())
  {
    return (action_it->second)(this, data);
  }
  else
  {
    std::cout << "Unknown message type: " << msg_type << std::endl;
    return { make_ping() };
  }
}

game_logic::msg_vector game_logic::on_join(const jsoncons::json& data)
{
  std::cout << "Joined" << std::endl;
  return { make_ping() };
}

game_logic::msg_vector game_logic::on_game_start(const jsoncons::json& data)
{
 
  std::cout << "Race started" << std::endl;
  return { make_ping() };
}

VectorXf game_logic::getInputVector() {
  int cPieceId = currentPosition.pieceID;
  auto cP = track.getPiece(cPieceId);
  auto nP = track.getPiece((cPieceId + 1) % track.getNumberOfPieces());
  VectorXf throttle_controller_input(23);
  throttle_controller_input[0] = 1.0f;
  throttle_controller_input[1] = cP.length;
  throttle_controller_input[2] = (cP.isBend ? 1 : -1);
  throttle_controller_input[3] = cP.angle;
  throttle_controller_input[4] = cP.radius;
  throttle_controller_input[5] = (cP.hasSwitch ? 1 : -1);
  throttle_controller_input[6] = nP.length;
  throttle_controller_input[7] = (nP.isBend ? 1 : -1);
  throttle_controller_input[8] = nP.angle;
  throttle_controller_input[9] = nP.radius;
  throttle_controller_input[10] = (nP.hasSwitch ? 1 : -1);
  throttle_controller_input[11] = car.length;
  throttle_controller_input[12] = car.width;
  throttle_controller_input[13] = car.guideFlagPosition;
  throttle_controller_input[14] = currentPosition.inDistance;
  throttle_controller_input[15] = currentPosition.angle;
  throttle_controller_input[16] = speed;

  int laneID = currentPosition.laneID;
  int laneDist = track.laneDistanceFromCentre(laneID);

  if (laneID - 1 >= track.getLeftLaneId()) {
    throttle_controller_input[17] = track.laneDistanceFromCentre(laneID - 1);
  } else {
    throttle_controller_input[17] = laneDist;
  }

  throttle_controller_input[18] = laneDist;

  if (laneID + 1 <= track.getRightLaneId()) {
    throttle_controller_input[19] = track.laneDistanceFromCentre(laneID + 1);
  } else {
    throttle_controller_input[19] = laneDist;
  }

  throttle_controller_input[20] = 0.0f;
  throttle_controller_input[21] = 0.0f;
  throttle_controller_input[22] = 0.0f;
  return throttle_controller_input;
}

void game_logic::update_positions_and_speed(const jsoncons::json &data) {
  lastTickSpeedMeasured = currentTick;
  position oldPosition = currentPosition;
  for (size_t i = 0; i < data.size(); ++i) {
    const jsoncons::json &datum = data[i];
    const jsoncons::json &id = datum["id"];
    if (id["name"].as<std::string>() != car.name ||
        id["color"].as<std::string>() != car.color)
      continue;

    double angle = datum["angle"].as<double>();

    const jsoncons::json &piecePosition = datum["piecePosition"];
    int pieceIndex = piecePosition["pieceIndex"].as<int>();
    double inPieceDistance = piecePosition["inPieceDistance"].as<double>();

    const jsoncons::json &lane = piecePosition["lane"];

    int laneID = lane["endLaneIndex"].as<int>();

    double dist =
        track.distance(oldPosition.pieceID, oldPosition.inDistance, pieceIndex,
                       inPieceDistance, oldPosition.laneID, laneID);
    speed = dist / (currentTick - lastTickSpeedMeasured);
    currentPosition = { laneID, pieceIndex, inPieceDistance, angle };
    break;
  }
}

game_logic::msg_vector
game_logic::on_car_positions(const jsoncons::json &data) {
  std::cout << data << std::endl;
  position oldPosition = currentPosition;
  update_positions_and_speed(data);

  unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();

  std::mt19937 generator(seed);
  std::uniform_real_distribution<float> distribution(-1.0f, 1.0f);

  auto throttle_controller_input = getInputVector();
  float t_change =
      (throttle_controller.regress(throttle_controller_input)[0] - 0.5f) * 2;

  if ( distribution(generator) < 0.0f){
    t_change = clamp(t_change + distribution(generator), -1.0f, 1.0f);
  }

  if (lap > 1 || currentPosition.pieceID > 1) {
    if (currentPosition.pieceID != oldPosition.pieceID) {
        //add best times to track
      if (currentPosition.pieceID % 2 == 0) {
        int ticks = currentTick - time0;
//reward here is faster than time of piece + previous
        tick0 = currentTick;
      } else {
        int ticks = currentTick - time1;

//reward here is faster than time of piece + previous
        tick1 = currentTick;
      }
    }
  }

  VectorXf out(1);
  out[0] = t_change;
  previous_states0.push_back(std::make_pair(throttle_controller_input, out);
  previous_states1.push_back(std::make_pair(throttle_controller_input, out);


  std::cout << "Throttle change: " << t_change << std::endl;
  throttle = clamp(throttle + t_change);

  return { make_throttle(throttle) };
}

game_logic::msg_vector game_logic::on_crash(const jsoncons::json& data)
{
  std::cout << "Someone crashed" << std::endl;
  if (data["name"].as<std::string>() != car.name ||
      data["color"].as<std::string>() != car.color) {
      //Punish and make sure that times are not mangled
    }
  return { make_ping() };
}

game_logic::msg_vector game_logic::on_game_end(const jsoncons::json& data)
{
  /*
   *trainData.close();
   */
  std::cout << "Race ended" << std::endl;
  throttle_controller.save_network("network");
  return { make_ping() };
}

game_logic::msg_vector game_logic::on_error(const jsoncons::json& data)
{
  std::cout << "Error: " << data.to_string() << std::endl;
  /*
   *trainData.close();
   */
  return { make_ping() };
}

game_logic::msg_vector game_logic::on_game_init(const jsoncons::json &data) {
  std::cout << "Received gameInit" << std::endl;
  track = std::move(Track(data));
  const auto& cars = (data["race"])["cars"];
  for (size_t i = 0; i < cars.size(); ++i) {
    const jsoncons::json &carj = cars[i];
    const jsoncons::json &id = carj["id"];
    if (id["name"].as<std::string>() != car.name ||
        id["color"].as<std::string>() != car.color)
      continue;

    const auto& dimensions = carj["dimensions"];
    car.length = dimensions["length"].as<double>();
    car.width = dimensions["width"].as<double>();
    car.guideFlagPosition = dimensions["guideFlagPosition"].as<double>();
    break;
  }

  currentPosition.laneID = 0;
  currentPosition.inDistance = 0.0;

  return { make_ping() };
}

game_logic::msg_vector game_logic::on_your_car(const jsoncons::json &data) {
  car.name = data["name"].as<std::string>();
  car.color = data["color"].as<std::string>();
  return { make_ping() };
}

inline float game_logic::clamp(float input, float down, float up) const {
  return fmin(up, fmax(input, down));
}
