#include <fstream>
#include <iostream>
#include <vector>
#include <string>
#include <utility>
#include <Eigen/Dense>
#include <Eigen/Core>
#include "mlp.h"
using namespace std;
using Eigen::VectorXf;


vector<pair<VectorXf, VectorXf>> loadData(){
    ifstream input("trainData");
    vector<pair<VectorXf, VectorXf> > data;
    for (int i = 0; i < 8565; ++i) {
      VectorXf datum(23);
      datum[0] = 1.0f;
      for (int j=1; j <=22 ; ++j){
          float a;
          input >> a;
          datum[j] = a;
      }
      float output;
      input >> output;
      output = output / 2 + 0.5f;
      VectorXf out(1);
      out[0] = output;
      data.emplace_back(make_pair(datum,out));
    }
    input.close();
    return data;
}


int main(int argc, const char* argv[]){

    int hidden = atoi(argv[1]);
    float rate = atof(argv[2]);
    int epochs = atoi(argv[3]);
    
    cout << "Hidden neurons: " << hidden << endl << "Learning rate: " << rate
         << endl << "Epochs : " << epochs << endl << endl;


    auto data = loadData();
    MLP network(22, hidden, 1, rate);

    network.train(data, epochs);

    network.save_network("network");

}
