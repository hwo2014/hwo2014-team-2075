#include "mlp.h"

MLP::MLP(int inputs, int hidden, int outputs, float n)
    : inputs{inputs},
      hidden{hidden},
      outputs{outputs},
      learning_rate{n},
      epochs{0} {
  unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();

  srand(seed);
  Wh = MatrixXf::Random(hidden + 1, inputs + 1);
  Wo = MatrixXf::Random(outputs, hidden + 1);

  Wh.row(0).setZero();
  Wh(0, 0) = 1;
   
  DWh = MatrixXf::Zero(hidden + 1, inputs + 1);
  DWo = MatrixXf::Zero(outputs, hidden + 1);
}

MLP::MLP(string filename) {
  ifstream ifs{filename};
  ifs >> inputs;
  ifs >> hidden;
  ifs >> outputs;
  ifs >> epochs;
  ifs >> learning_rate;

  Wh = MatrixXf(hidden + 1, inputs + 1);
  Wo = MatrixXf(outputs, hidden + 1);

  for (int i = 0; i < hidden + 1; ++i)
    for (int j = 0; j < inputs + 1; ++j) 
      ifs >> Wh(i, j);

  for (int i = 0; i < outputs; ++i)
    for (int j = 0; j < hidden + 1; ++j) 
      ifs >> Wo(i, j);
}

VectorXf MLP::activation(VectorXf &u){
  return ((-u).array().exp().matrix() + VectorXf::Ones(u.size()))
      .cwiseInverse();
}

VectorXf MLP::forward(const VectorXf &x) {
  VectorXf u;
  u.noalias() = Wh * x;
  Yh = activation(u);
  u.noalias() = Wo * Yh;
  return activation(u);
}


void MLP::backward(const VectorXf &desired, const VectorXf &output,
                   const MatrixXf &input) {
  Do = output.cwiseProduct(VectorXf::Ones(output.size()) - output)
           .cwiseProduct(desired - output);

  Dh = Yh.cwiseProduct(VectorXf::Ones(Yh.size()) - Yh)
           .cwiseProduct(Wo.transpose() * Do);

  DWo = learning_rate * Do * Yh.transpose();

  Wo += DWo;

  Wh.noalias() += learning_rate * (Dh * input.transpose());
}

int maxPosition(const VectorXf &x) {
  int i;
  x.maxCoeff(&i);
  return i;
}

int MLP::classify(const VectorXf &input) { return maxPosition(forward(input)); }

VectorXf MLP::regress(const VectorXf &input) { return forward(input); }

void MLP::train(vector<pair<VectorXf, VectorXf> > &traindata, int epochs) {

  unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();

  std::mt19937 generator(seed);

  for (int i = 0; i < epochs; ++i) {
    shuffle(traindata.begin(), traindata.end(), generator);

    for (const auto &train : traindata) {
      /*
       *VectorXf norm = (train.first.array() / 255.0f).matrix();
       *norm[0] = 1.0f;
       */
      auto output = forward(train.first);

      backward(train.second, output, train.first);
    }
   
    error_train.push_back(MSE(traindata));
    cout << "MSE after epoch " << this->epochs + i + 1 << endl
         << "On train data " << *(error_train.end() - 1) << endl;
  }
  this->epochs += epochs;
}

float MLP::MSE(const vector<pair<VectorXf, VectorXf> > &data) {
  float e{0.0f};
  for (const auto &test : data) {
    /*
     *VectorXf norm = (test.first.array() / 255.0f).matrix();
     *norm[0] = 1.0f;
     */
    auto output = forward(test.first);
    e += (output - test.second).squaredNorm();
  }
  return e / data.size();
}

void MLP::save_network(string filename){
    ofstream ofs{filename};
    ofs << inputs << " " << hidden << " " << outputs << " " << epochs << " "
        << learning_rate << endl;
    for (int i = 0; i < hidden + 1; ++i){
      for (int j = 0; j < inputs + 1; ++j) ofs << Wh(i, j) << " ";
      ofs << endl;
    }

    for (int i = 0; i < outputs; ++i){
      for (int j = 0; j < hidden + 1; ++j) ofs << Wo(i, j) << " ";
      ofs << endl;
    }
}
